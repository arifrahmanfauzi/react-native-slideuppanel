import React from "react";
import { Text, View, Dimensions, Image, Animated } from "react-native";

import SlidingUpPanel from "rn-sliding-up-panel";

const { height } = Dimensions.get("window");

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#f8f9fa",
    alignItems: "center",
    justifyContent: "center"
  },
  bottomContainer:{
    flex:1,
    alignItems: 'center',
  },
  panel: {
    flex: 1,
    backgroundColor: "white",
    position: "relative",
    zIndex:1,
  },
  panelHeader: {
    height: 5,
    backgroundColor: "#b197fc",
    justifyContent: "center",
    alignItems: 'center',
    padding: 24
  },
  textHeader: {
    fontSize: 28,
    color: "#FFF"
  },
};

class App extends React.Component {
  static defaultProps = {
    draggableRange: { top: height + 180 - 64, bottom: 70 }
    //draggableRange: { top: height + 180 - 64, bottom: 180 }
  };

  _draggedValue = new Animated.Value(45);

  render() {
    //const { top, bottom } = this.props.draggableRange;
    return (
      <View style={styles.container}>
        <Text onPress={() => this._panel.show(300,{velocity:0.9})}>Show panel</Text>
        <SlidingUpPanel
          ref={c => (this._panel = c)}
          // draggableRange={this.props.draggableRange}
          animatedValue={this._draggedValue}// awal panel buka
          snappingPoints={[360]}
          //height={height/2} -> tinggi panel body yang putih
          friction={0.5}
        >
            <View style={styles.panel}>
            <View style={styles.panelHeader}>
              <Text>This header</Text>
            </View>
            {/* end panel header */}
            <View style={styles.bottomContainer}>
              <Text>Bottom sheet content</Text>
            </View>
          </View>
        </SlidingUpPanel>
      </View>
    );
  }
}

export default App;
